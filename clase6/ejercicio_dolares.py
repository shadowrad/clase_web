import requests
from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from clase6.diego_url import  key as api_key
import json

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)
usuarios = [{"nombre": "B", "mail": "C"}, {"nombre": "otronom", "mail": "rodrigo@mail_algo.com"}, {"nombre": "rodrig", "mail": "rodrig@mail_algo.com"}]

class Dolar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    fecha = db.Column(db.String(80))
    valor = db.Column(db.Float)

    def __repr__(self):
        return str(self.valor)

db.create_all()


@app.route('/creardolares',methods=['GET'])
def crear_dolares():
    url = "https://apis.datos.gob.ar/series/api/series?ids=168.1_T_CAMBIOR_D_0_0_26"
    respuesta = requests.get(url).json()
    for dato in respuesta['data']:
        dolar = Dolar()
        dolar.fecha = dato[0]
        dolar.valor= dato[1]
        db.session.add(dolar)
        db.session.commit()
    return jsonify({"resultado": 'ok'})

@app.route('/listar',methods=['GET'])
def listar_usuario():
    dolares = Dolar.query.all()
    dolar_dict =[]
    for dolar in dolares:
        dolar_dict.append({'fecha':dolar.fecha,'valor':dolar.valor})
    return jsonify(dolar_dict)

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, port=3000)


