from datetime import datetime

import requests
from flask import Flask, jsonify, request, render_template
from flask_sqlalchemy import SQLAlchemy
import json

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

class Persona(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(80))
    apellido  = db.Column(db.String(80))
    edad = db.Column(db.Integer)
    def __repr__(self):
        return str(self.nombre)

db.create_all()


@app.route('/angular',methods=['GET'])
def angular():
    return render_template('angular.html')

@app.route('/jquey',methods=['GET'])
def jqueyView():
    return render_template('ej2_ajax.html')

@app.route('/listarPersona',methods=['GET'])
def listar_personas():
    personas = Persona.query.all()
    persona_dict =[]
    for persona in personas:
        persona_dict.append({'id':persona.id,'apellido':persona.apellido,'nombre':persona.nombre, 'edad': persona.edad})
    return jsonify({'lista':persona_dict})

@app.route('/crearPersona',methods=['POST'])
def crear_persona():
    persona = Persona()
    persona_json= request.get_json()
    persona.nombre= persona_json['nombre']
    persona.apellido= persona_json['apellido']
    persona.edad = persona_json['edad']
    db.session.add(persona)
    db.session.commit()
    return jsonify({"resultado": 'ok'})

@app.route('/updatePersona',methods=['POST'])
def modificar_persona():
    persona_json= request.get_json()
    persona= Persona.query.get(persona_json['id'])
    persona.nombre= persona_json['nombre']
    persona.apellido= persona_json['apellido']
    persona.edad= persona_json['edad']
    db.session.add(persona)
    db.session.commit()
    return jsonify({"resultado": 'ok'})



@app.route('/deletePersona',methods=['POST'])
def delete_persona():
    persona_json = request.get_json()
    personas = Persona.query.filter_by(id= persona_json['id'])
    for persona in personas:
        db.session.delete(persona)
        db.session.commit()
    return jsonify({"resultado": 'ok'})

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, port=3000)


