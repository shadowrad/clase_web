from flask import Flask
from flask import render_template
from flask import request
app = Flask(__name__)


@app.route('/')
def hola_mundo():
    return 'hola mundo'


@app.route('/vista',methods=['GET'])
def vista():
    dato = request.args['nombre']
    return render_template('home.html',nom=dato)



@app.route('/prueba2',methods=['POST'])
def posteo():
    return 'nombre '+request.form['nombre']


@app.route('/prueba/<string:nombre>',methods=['POST'])
def posteo2(nombre):
    return nombre



if __name__ == '__main__':
    app.run(host="0.0.0.0",debug=True, port=3000)    