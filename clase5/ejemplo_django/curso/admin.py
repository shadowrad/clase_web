from django.contrib import admin
from curso.models import Curso, Alumno

class AlumnoInline(admin.TabularInline):
    model = Alumno

class CursoAdmin(admin.ModelAdmin):
    inlines = [AlumnoInline]



# Register your models here.
admin.site.register(Curso,CursoAdmin)
