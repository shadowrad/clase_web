import requests
from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
import json

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)
usuarios = [{"nombre": "B", "mail": "C"}, {"nombre": "otronom", "mail": "rodrigo@mail_algo.com"}, {"nombre": "rodrig", "mail": "rodrig@mail_algo.com"}]

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)

    def __repr__(self):
        return '<User %r>' % self.username
db.create_all()




@app.route('/user/apicrear',methods=['GET'])
def crear_usuario_desde_api():
    users= usuarios
    for u in users:
        usuario = User()
        usuario.username = u['nombre']
        usuario.email =u['mail']
        db.session.add(usuario)
        db.session.commit()
    return jsonify({"resultado":'ok'})




@app.route('/user/crear/<string:username>',methods=['GET'])
def crear_usuario(username):
    usuario = User()
    usuario.username = username
    usuario.email =username+'@mail_algo.com'
    db.session.add(usuario)
    db.session.commit()
    return jsonify({"resultado":'ok'})


@app.route('/user/modificar/<string:username>/<string:otro>',methods=['GET'])
def modificar_usuario(username,otro):
    usuario = User.query.filter_by(username=username).first()
    usuario.username = otro
    db.session.add(usuario)
    db.session.commit()
    return jsonify({"resultado": 'ok'})

@app.route('/user/listar',methods=['GET'])
def listar_usuario():
    usuarios = User.query.all()
    usuarios_dict =[]
    for u in usuarios:
        usuarios_dict.append({'nombre':u.username,'mail':u.email})
    return json.dumps(usuarios_dict)


@app.route('/user/eliminar/<string:username>',methods=['GET'])
def eliminar_usuario(username):
    usuario = User.query.filter_by(username=username).first()
    db.session.delete(usuario)
    db.session.commit()
    return jsonify({"resultado": 'ok'})





if __name__ == '__main__':
    app.run(host="0.0.0.0",debug=True, port=3000)