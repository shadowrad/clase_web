from datetime import datetime

import requests
from flask import Flask, jsonify, request, render_template
from flask_sqlalchemy import SQLAlchemy
import json

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)
usuarios = [{"nombre": "B", "mail": "C"}, {"nombre": "otronom", "mail": "rodrigo@mail_algo.com"}, {"nombre": "rodrig", "mail": "rodrig@mail_algo.com"}]

class Dolar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    fecha = db.Column(db.String(80))
    valor = db.Column(db.Float)

    def __repr__(self):
        return str(self.valor)
db.create_all()

@app.route('/vista',methods=['GET'])
def vista():
    dato = request.args['nombre']
    return render_template('home.html',nom=dato)

@app.route('/jquery',methods=['GET'])
def jquery():
    return render_template('ej1_jquery.html')

@app.route('/ajax',methods=['GET'])
def ajax():
    return render_template('ej2_ajax.html')


def listar_dolares():
    dolares = Dolar.query.all()
    dolar_dict =[]
    for dolar in dolares:
        dolar_dict.append({'fecha':dolar.fecha,'valor':dolar.valor})
    return dolar_dict

@app.route('/crearDolar',methods=['POST'])
def crear_dolar():
    dolar = Dolar()
    dolar.fecha = datetime.now()
    dolar.valor= request.form['valor']
    db.session.add(dolar)
    db.session.commit()
    lista=listar_dolares()
    return jsonify({"resultado": 'ok','lista':lista})

@app.route('/deleteDolar',methods=['POST'])
def delete_dolar():
    dolares = Dolar.query.filter_by(valor= request.form['valor'])
    for dolar in dolares:
        db.session.delete(dolar)
        db.session.commit()
    lista=listar_dolares()
    return jsonify({"resultado": 'ok','lista':lista})


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, port=3000)


