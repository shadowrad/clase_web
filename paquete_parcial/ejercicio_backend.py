from datetime import datetime

import requests
from flask import Flask, jsonify, request, render_template
from flask_sqlalchemy import SQLAlchemy
import json

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)


class Ejemplo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(80))

    def __repr__(self):
        return str(self.nombre)


db.create_all()


@app.route('/', methods=['GET'])
def angular():
    return render_template('angular.html')


@app.route('/listarEjemplo', methods=['GET'])
def listar_ejemplos():
    ejemplos = Ejemplo.query.all()
    ejemplo_dict = []
    for ejemplo in ejemplos:
        ejemplo_dict.append({'id': ejemplo.id, 'descripcion': ejemplo.descripcion})
    return jsonify({'lista': ejemplo_dict})


@app.route('/crearEjemplo', methods=['POST'])
def crear_ejemplo():
    ejemplo = Ejemplo()
    ejemplo_json = request.get_json()
    ejemplo.descripcion = ejemplo_json['descripcion']
    db.session.add(ejemplo)
    db.session.commit()
    return jsonify({"resultado": 'ok'})


@app.route('/updateEjemplo', methods=['POST'])
def modificar_ejemplo():
    ejemplo_json = request.get_json()
    ejemplo = Ejemplo.query.get(ejemplo_json['id'])
    ejemplo.descripcion = ejemplo_json['descripcion']
    db.session.add(ejemplo)
    db.session.commit()
    return jsonify({"resultado": 'ok'})


@app.route('/deleteEjemplo', methods=['POST'])
def delete_ejemplo():
    ejemplo_json = request.get_json()
    ejemplos = Ejemplo.query.filter_by(id=ejemplo_json['id'])
    for ejemplo in ejemplos:
        db.session.delete(ejemplo)
        db.session.commit()
    return jsonify({"resultado": 'ok'})


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, port=3000)
